# jolt: john's own lisp-y thing

## Examples
Fibonacci
  # Returns a list of Fibonacci terms, starting with a and b, up to n in 
  # length, for n > 2.
  (def fibs (
    (def-args ((a Int) (b Int) (n Int)) (
      !(if !(< n 2)
        ((warn "n should be at least 2")
         (def n 2))
        ())
      (def v !(Vector n. Int))
      (set v.0 a.)
      (set v.1 b.)
      (for i !(range 2 n.) (set v.i !(+ v.!(- i 2). v.!(- i 1).)))
      (return v.)))))
