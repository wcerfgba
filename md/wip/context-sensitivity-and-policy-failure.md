Context sensitivity and policy failure
======================================

A hard policy can be considered as a set of prescriptions that govern some 
social mechanism so as to achieve some desired outcome:
  * Drug control laws specify how certain substances can be acquired -- if at 
    all -- so as to limit harm to people in society caused by the direct (e.g. 
    overdose) and indirect (e.g. anti-social behaviour) consequences of drug 
    use.

Policy must define a context in order to be applicable:
  * Drug control legislation typically operates within the context of the 
    possession, production, and distribution of drugs.

Elements in the 'real life context' of some event to which a policy pertains 
can affect the validity of the policy, either in its effectiveness or in the 
moral correctness of its goal -- these may be considered as the externalities 
of the policy:
  * Some people use drugs in ways that are not harmful to themselves or people 
    around them. In this instance it would be immoral to restrict their 
    personal use.

The context specified in a policy will always be a strict subset of the 'real 
life context' in which the events to which the policy pertains take place:
  * Drug cotrol legislation cannot cover the myriad reasons people use drugs 
    in the first place.

Therefore it is almost certain that every policy will have externalities, 
and thus hard policies are always open to failure:
  * By defining hard rules that restrict access to drugs, there will always 
    be instances where lack of access prevents a morally correct use of those 
    drugs.

A soft policy, in contrast to a hard policy, consists of guidelines, rather 
than rules:
  * We might consider "drugs can cause addiction, mental health issues, and as 
    results of these, anti-social behavior" as a primitive soft policy.
      * Notice that this, instead of talking about _how_ drugs can be 
        controlled, instead talks about the _effects_ they can have.

Soft policies are less vulnerable to failure, because they are more open to 
incorporating elements of the wider 'real life context' than their hard 
counterparts:
  * Consider the case where someone is found in possession of a drug: under 
    the soft policy we are invited to consider the behaviour and state of mind 
    of the person in question, whereas under the hard policy we are 'told' 
    that possession in itself is morally wrong or must be dealt with in some 
    particular manner.

--

Do soft policies necessitate different social mechanisms for their enactment?
