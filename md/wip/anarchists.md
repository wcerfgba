

The first anarchists must have been anarchists-in-heart. Across many cultures, there have been people who have discussed the Good Life, the best way to live. Anarchism presents itself as one answer to this question, in the form of a solution to the existential dilemma of lack of ultimate meaning for one's life, by asserting the right of individuals to live their lives by their own accord. The anarchist-in-heart feels propelled to lead their life to the fullest [c.f. self-actualisation and related theories] -- in Oscar Wilde's words, to "live", rather than simply to "exist".

