﻿## Power
Power can be individual or social.

### Individual power
Individual power stems from the innate abilities that an individual possesses: I can think, and thus can formulate ideas; I have a human body, and thus can move myself and other things in the world, according to its physical limits; I feel emotions, and so am directed towards certain thoughts and actions.

These abilities define and limit the scope of our power.

Individual power covers the entire domain of potential of the individual, and gives temporality to the concept of the individual.






### Social power
Social power is based on our willingness as individuals to exercise our individual power according to 






Most of the time, individual power is leveraged volitionally; we do the things we want to do, or at least choose to do. 








Authority








I am more than a concept!