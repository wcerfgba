Personal manifesto
==================

All values are inherently subjective/relative; good and bad are human 
constructs and the judgement of some particular act will vary across persons 
and communities.

Due to subjectivism, it is immoral to impose one's morality on someone else, 
as this violates their freedom to have their own morality.

Although ultimately subjective, this meta-ethical principle of 'freedom' must 
be adopted by all m
