What is stopping us from building the world we want to live in?
  Lack of solidarity.
    The individual alone is weak: he possesses only one person worth of real 
    power.
      Natural law -- inevitable law -- is decided by real power: if the 
      members of a community live as they please in harmony with themselves 
      and resist any attempts from the outside to dissolve them, then their 
      collective will shall be the one that rules them.

    What solidarity exists today? What solidarity has died out?

    How can we construct more?
      How do we deal with globalisation and achieve global solidarity?




  

With global solidarity, the power of humanity is now sufficient to undertake 
global change: now there is nothing stopping people.
  We need a well-defined idea of what we want the world to be like.
  
  We need to analyze the world as it is now, so we can understand why existing
  social mechanisms have failed us.

  We need to find the routes that will take us from where we are to where we 
  want to be.
    This may take on the form of proposals for new social mechanisms to 
    replace the old ones.

  We must have access to the resources needed to follow the paths.

  Construction begins.
