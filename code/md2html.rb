# A simple Ruby script to recursively generate appropriately-named HTML 
# versions of all of the Markdown files in a directory, wrapping them as 
# HTML documents and linking in a stylesheet.

require "kramdown"
require "fileutils"

## Site URL for links.
$site_url = "http://localhost:8000"

## Strings in template to be replaced with sitemap and rendered Markdown
## respectively.
$rendered_anchor = /<!-- md2html_rendered -->/
$sitemap_anchor = /<!-- md2html_sitemap -->/

begin
  template_file = ARGV.fetch(0)
  includes_dir = ARGV.fetch(1)
  input_dir = ARGV.fetch(2)
  output_dir = ARGV.fetch(3)
rescue IndexError
  warn "Need four arguments: <template_file> <includes_dir> <input_md_dir> <output_html_dir>"
  exit
end

$template = File.open(template_file).read


def build_sitemap(input_dir)
  input_dir_p1 = input_dir[/^.*\/(.*)/, 1]
 
  html = '<div class="sitemap_level">'

  Dir.foreach(input_dir) { |filename|
    path = "#{input_dir}/#{filename}"

    if File.directory? path and not filename.start_with? "."
      print "Mapping subdirectory: #{path}\n"
      
      html << "<p class=\"sitemap_link\">#{filename}/</p>"
      html << build_sitemap(path)
    elsif not filename.start_with? "."
      print "Mapping file: #{path}\n"
      
      html << "<a href=\"#{$site_url}/#{input_dir_p1}/#{filename}\" class=\"sitemap_link\">#{filename}</a>"
    end
  }

  html << '</div>'

  return html
end

def render_markdown(input_dir, output_dir)
  Dir.foreach(input_dir) { |filename|
    path = "#{input_dir}/#{filename}"

    if File.directory? path and not filename.start_with? "."
      print "Rendering subdirectory: #{path}\n"

      subdir = "#{output_dir}/#{filename}"

      if not Dir.exists? subdir 
        Dir.mkdir(subdir)
      end

      render_markdown(path, subdir)
    elsif filename.end_with? ".md" and not filename.start_with? "."
      print "Rendering file: #{path}\n"
     
      md = File.open(path).read
      html = $template.sub($rendered_anchor,
                           Kramdown::Document.new(md, :input => "markdown")
                                             .to_html)
      File.open("#{output_dir}/#{filename[/(.*)\.md$/, 1]}.html", "w") { |f|
        f.write(html)
      }
    end
  }
end

def insert_sitemap(output_dir)
  Dir.foreach(output_dir) { |filename|
    path = "#{output_dir}/#{filename}"

    if path.end_with? ".html" and not filename.start_with? "."
      print "Inserting sitemap: #{path}\n"
      
      html = File.open(path).read
      html.sub!($sitemap_anchor, $sitemap)
      File.open(path, mode="w").write(html)
    elsif File.directory? path and not filename.start_with? "."
      print "Inserting in subdirectory: #{path}\n"
      
      insert_sitemap(path)
    end
  }
end


render_markdown(input_dir, output_dir)
FileUtils::cp_r("#{includes_dir}/.", output_dir)
$sitemap = build_sitemap(output_dir)
insert_sitemap(output_dir)
